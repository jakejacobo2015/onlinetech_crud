<!doctype HTML>
<html>
    <head>
        <title> Online Tech</title>
        <meta charset="UTF-8">
        <meta name="Online Tech" content="Web Tutorial">
        <meta name="keywords" content="PHP,MySQL">
        <meta name="author" content="Jake Jacobo">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1> Signup Form </h1>

        <form action="./create.php" method="post" name="signupform">
            First Name:<br>
            <input type="text" name="fullname" id="fullname" class="fullname" placehold="Full Name" required><br>
            Age:<br>
            <input type="number" name="age" id="age" class="age" placehold="age" required><br>
            Address:<br>
            <textarea name="address" id="address" class="address" placehold="Full Name" cols="24" rows="12" required></textarea><br>
            <br>
            <input type="submit" value="Save" name="save">
        </form>

    
    <br><br><br>

    
        <table border="1" width="800">
            <tr>
                <td colspan="5" style="text-align:center;">
                    <h1>User Table</h1>
                </td>
            </tr>
            <tr style="text-align:center;font-size:20px;">
                <th> Fullname </th> 
                <th> Age </th> 
                <th> Address </th>
                <th colspan="2"> Action </th>
            </tr>

                    <?php
                        include_once './db_config.php';
                        $db_connection = mysqli_connect($dbHost,$dbUsername,$dbUserPassword,$dbName);
                        if($db_connection === false){
                            die("ERROR: Could not connect. " . mysqli_connect_error());
                        }
                        $query_result = mysqli_query($db_connection, "SELECT * FROM online_tech_tbl");

                        while ($query_row = mysqli_fetch_array($query_result)) { 
                    ?>

                            <tr>
                                <td style="text-align:center;"><?php echo $query_row['db_col_fullname']; ?></td>
                                <td style="text-align:center;"><?php echo $query_row['db_col_age']; ?></td>
                                <td style="text-align:center;"><?php echo $query_row['db_col_address']; ?></td>
                                <td style="text-align:center;"><a href="./update.php?db_col_uid=<?php echo $query_row['db_col_uid'];?>">Edit</td>
                                <td style="text-align:center;"><a href="./delete.php?db_col_uid=<?php echo $query_row['db_col_uid'];?>"  onclick="return confirm('Are your sure you want to delete: <?php echo $query_row['db_col_fullname'];?> ?')">Delete</td>
                            </tr>

                    <?php } ?>
        </table>
    </body>
</html>