-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2019 at 07:56 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_tech_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `online_tech_tbl`
--

CREATE TABLE `online_tech_tbl` (
  `db_col_uid` int(255) NOT NULL,
  `db_col_fullname` varchar(255) NOT NULL,
  `db_col_age` smallint(255) NOT NULL,
  `db_col_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `online_tech_tbl`
--

INSERT INTO `online_tech_tbl` (`db_col_uid`, `db_col_fullname`, `db_col_age`, `db_col_address`) VALUES
(12, 'Jake', 0, 'O.C');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `online_tech_tbl`
--
ALTER TABLE `online_tech_tbl`
  ADD PRIMARY KEY (`db_col_uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `online_tech_tbl`
--
ALTER TABLE `online_tech_tbl`
  MODIFY `db_col_uid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
