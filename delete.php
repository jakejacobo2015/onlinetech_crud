<?php
       
        //DISPLAY DATA PER UID SELECTED
        if(isset($_GET["db_col_uid"]) && !empty(trim($_GET["db_col_uid"])) && $_SERVER["REQUEST_METHOD"] == "GET"){

                include_once './db_config.php';
                $db_connection = mysqli_connect($dbHost,$dbUsername,$dbUserPassword,$dbName);
                if($db_connection === false){
                    die("ERROR: Could not connect. " . mysqli_connect_error());
                }

                $db_col_uid = $_GET['db_col_uid'];
                $query_result = mysqli_query($db_connection, "DELETE FROM online_tech_tbl WHERE db_col_uid=$db_col_uid");

                if ($query_result){
                    header("location:./");
                    exit();
                }else{
                    die("ERROR: Could Not Execute. " . mysqli_connect_error());
                }
        }

?>