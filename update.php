<?php
include './db_config.php';



//UPDATE OPERATION
if(isset($_POST["db_col_uid"]) && !empty($_POST["db_col_uid"])){ 

    $db_connection = mysqli_connect($dbHost,$dbUsername,$dbUserPassword,$dbName);
    if($db_connection === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }

    $db_col_uid = $_POST['db_col_uid'];
    $fullname = (empty($_POST["fullname"])) ? "0" : trim($_POST["fullname"]);
    $age = (empty($_POST["age"])) ? "0" : trim($_POST["age"]);
    $address = (empty($_POST["address"])) ? "0" : trim($_POST["address"]);

    $query_result = mysqli_query($db_connection, "UPDATE online_tech_tbl SET db_col_fullname='$fullname', db_col_age='$age' , db_col_address='$address' WHERE db_col_uid=$db_col_uid");  
  
    if ($query_result){
        header("location:./");
        exit();
    }else{
        die("ERROR: Could Not Execute. " . mysqli_connect_error());
        header("location:./");
        exit();
    }

}
?>



<!doctype HTML>
<html>
    <head>
        <title>Edit Form</title>
        <meta charset="UTF-8">
        <meta name="Online Tech" content="Web Tutorial">
        <meta name="keywords" content="PHP,MySQL">
        <meta name="author" content="Jake Jacobo">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1> Signup Form </h1>

        <?php
        //DISPLAY DATA PER UID SELECTED
        if(isset($_GET["db_col_uid"]) && !empty(trim($_GET["db_col_uid"]))){

 
                $db_connection = mysqli_connect($dbHost,$dbUsername,$dbUserPassword,$dbName);
                if($db_connection === false){
                    die("ERROR: Could not connect. " . mysqli_connect_error());
                }
                
                $db_col_uid = $_GET['db_col_uid'];
                $query_result = mysqli_query($db_connection, "SELECT * FROM online_tech_tbl WHERE db_col_uid=$db_col_uid");

         
                    
                if (count($query_result) == 1 ) {
                     $query_row = mysqli_fetch_array($query_result);
        ?>
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="signupform">
                                <input type="hidden" name="db_col_uid" id="db_col_uid" class="db_col_uid" value="<?php echo $query_row['db_col_uid']; ?>" required>
                                First Name:<br>
                                <input type="text" name="fullname" id="fullname" class="fullname" placehold="Full Name" value="<?php echo  $query_row['db_col_fullname'];?>" required><br>
                                Age:<br>
                                <input type="number" name="age" id="age" class="age" placehold="age" value="<?php echo  $query_row['db_col_age']; ?>"><br>
                                Address:<br>
                                <textarea name="address" id="address" class="address" placehold="Full Name" cols="40" rows="12" required><?php echo  $query_row['db_col_address']; ?></textarea><br>
                                <br>
                                <input type="submit" value="update" name="update">
                    </form>
        <?php
                }
        }
        ?>

    </body>
</html>


